#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# funciones para main.py
from jinja2 import TemplateNotFound, Environment, FileSystemLoader
import logging
import os


def generic_render_path(request, capture, context):
    if capture == None or capture == '/':
        template_file = 'index'
    else:
        template_file = capture.rstrip('/')

    try:
        request.render_template('paths/%s.jinja2' % template_file, **context)
    except TemplateNotFound, IOError:
        try:
            request.render_template('paths/%s/index.jinja2' % template_file, **context)
        except TemplateNotFound, IOError:
            request.abort(404)
            return

def handle_404(request, response, exception):
    logging.exception(exception)

    j2env = Environment(loader=FileSystemLoader('%s/templates' % os.path.dirname(__file__)))

    template = j2env.get_template('404.jinja2')
    template_values = {
        'url' : request.path,
        'site_title' : 'Yivoff'
    }

    response.write(template.render(template_values))
    response.set_status(404)