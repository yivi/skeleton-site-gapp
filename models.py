# -*- coding: utf-8 -*-
#
# Mmmodelos
__author__ = 'yivi'

from google.appengine.ext import db


class FortuneCookie(db.Model):
    line = db.StringProperty(required=True)
    source = db.StringProperty(required=False)
    randomness = db.FloatProperty(required=True)


