#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Micrositio genérico google apps.
from random import random
from google.appengine.ext import db
import webapp2
import logging
import os
import fortunes

from functions import generic_render_path, handle_404
from models import FortuneCookie

from webapp2_extras.routes import DomainRoute, RedirectRoute, PathPrefixRoute
from webapp2_extras import jinja2

template_values = {
    'site_title' : 'Yivoff'
}

class BaseHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def render_template(self, filename, **template_args):
        rendered_template = self.jinja2.render_template(filename, **template_args)
        self.response.write(rendered_template)


class MainHandler(BaseHandler):
    # mostly for single-use, static pages.
    def get(self, path=None):
        template_values['cookie'] = fortunes.cookies[int(round((len(fortunes.cookies)  - 1)* random()))]['line']

        generic_render_path(self, self.request.path, template_values)
    def post(self, path=None):
        # there shouldn't be any 'generic' posts, but hey, wtf.
        generic_render_path(self, self.request.path, template_values)

class AdminFortune(BaseHandler):
    def get(self, path=None):
        generic_render_path(self, self.request.path, template_values)

    def post(self, path=None):

        text = self.request.POST.get('cookie_text')
        lines = text.splitlines()

        batch_put = []
        for line in lines:
            if line.find('@@') > 0:
                (fortune, source) = line.split('@@')
            else:
                fortune = line
                source = None

            batch_put.append(FortuneCookie(line=fortune, source=source, randomness=random()))

        db.put(batch_put)
        template_values['lines'] = lines

        generic_render_path(self, self.request.path, template_values)




lines = (
    { 'line' : "I'm gonna get myself connected", 'source' : "Stero MCs" },
    { 'line' : "Got milk? Then you are a human and must be killed!", 'source' : "Futurama" },
    { 'line' : "The spirit is willing, but the flesh is spongy and bruised", 'source' : "Futurama" },
)


app = webapp2.WSGIApplication([
    RedirectRoute('/', handler=MainHandler),
    PathPrefixRoute('/admin', [
        RedirectRoute('/', handler=MainHandler, name='AdminRoot', strict_slash=True),
        RedirectRoute('/fortune', handler=AdminFortune, name="AdminFortune", strict_slash=True)
    ]),
    RedirectRoute('/<path:.*>', handler=MainHandler, name='default-route', strict_slash=True),
], debug=True)

app.error_handlers[404] = handle_404
